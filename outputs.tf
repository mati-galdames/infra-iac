# Define un recurso de salida llamado "cluster_name"
output "cluster_name" {
  description = "Cluster name" # Descripción del recurso de salida
  value       = module.gke.name  # Valor del recurso de salida, obtenido del módulo "gke" y su variable "name"
}