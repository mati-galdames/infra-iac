# Bloque de proveedor para Google Cloud Platform
provider "google" {
#   version = "1.20.0"  #  Especificar la versión del proveedor a utilizar
   credentials = "${file("./creds/serviceaccount.json")}"  # Cargar las credenciales de un archivo JSON
   project     = "my-firt-project-347213" # Reemplazar con el ID del proyecto de Google Cloud
   region      = "var.region"  # Se agrega region a traves del archivo de variables
   #region      = "us-central1"
 }