# Despliegue de cluster de kubernetes en GCP utilizando terraform como IaC y creando un despliegue en GKE
### Descripción

Este proyecto se enfocó en la creación de un cluster en GCP utilizando Terraform como herramienta de IaC. Con esta herramienta, se pudo automatizar la configuración y creación de recursos en GCP, lo que permitió una mayor eficiencia y facilidad en el proceso.

### Antes del Despliegue

Antes de realizar el despliegue se debe configurar 3 variables de entorno que nos ayudara con la comunicación con nuestro proyecto de GPC.
Las variables de entorno son las siguientes:

```GCLOUD_CLUSTER_NAME= demo-cluster```   --> Esta variable se extrae del archivo de terraform variable.tf del repositorio infra.iac

```GCLOUD_PROJECT_ID= my-firt-project-347213```   -->  Esta variable se obtiene del ID de nuestro proyecto creado en GCP.

```GCLOUD_SERVICE_KEY= ewogICJ0eXBlIjogInNlcnZpY2VfYWNjb3VudCIsCiAgInByb2plY3RfaWQiOiAibXktZmlydC1wcm9qZWN0LTM0NzIxMyIsCiAgI*************```   -->   Este es un estracto de la clave creada en GCP. Se debe crear en ```service-account``` en nuestro IAM del proyecto de GCP, aquí debemos generar una secret-key .json la cual ocuparemos como variable en nuestros proyectos.
Para este caso particular terrafom tuve inconvenientes con utilizar la key como archivo .json, por lo cual se convirtio en base64.




### Proceso de Despliegue

Para realizar el despliegue, es necesario seguir los siguientes pasos:

* Ingresar a nuestro proyecto de Gitlab a la ruta CI/CD > Pipeline -> https://gitlab.com/mati-galdames/infra-iac/-/pipelines
* Ejecutar el pipeline, esto realizará un proceso de validate, plan y apply. Los dos primeros se ejecutan automáticamente cuando detecta un cambio en los archivos del repositorio. En cambio, el proceso de apply es manual, esto da la seguridad de que en producción no pueda pasar algún error sin antes revisar los cambios en el proceso de plan.

En los archivos estan los comentarios de cada una las lineas de codigo ejecutadas en este despliegue.

Para la correcta ejecución debe cambiar los valores del archivo ```variables.tf ``` a sus valores correspondientes de su proyecto en GCP





