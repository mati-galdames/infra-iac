#terraform {
#  required_providers {
#    google = {
#      source = "hashicorp/google"
#      version = "4.47.0"
#    }
#  }
#}

# Modulo "gke_auth" para autenticar con el cluster de GKE
module "gke_auth" {
  source = "terraform-google-modules/kubernetes-engine/google//modules/auth" # Fuente del módulo es una ubicación específica en Github
  version = "24.1.0" # Versión del módulo
  depends_on   = [module.gke]  # Depende del módulo "gke"
  project_id   = var.project_id  # ID del proyecto definido como variable - ** Todas estas variables se encuentran en el archivo variables.tf **
  location     = module.gke.location # Ubicación definida por el módulo "gke"
  cluster_name = module.gke.name # Nombre del clúster definido por el módulo "gke"
}

# Define un recurso Terraform llamado "local_file" y "kubeconfig"
resource "local_file" "kubeconfig" {
  content  = module.gke_auth.kubeconfig_raw # content es una variable del módulo "gke_auth"
  filename = "kubeconfig-${var.env_name}" # Nombre del archivo junto a variable {var.env_name} 
}

# Define un módulo Terraform llamado "gcp-network"
module "gcp-network" {
  source       = "terraform-google-modules/network/google"  # Fuente del módulo es una ubicación específica en Github
  version      = "6.0.0"   # Versión del módulo
  project_id   = var.project_id  # ID del proyecto definido como variable
  network_name = "${var.network}-${var.env_name}"  # Nombre de la red con variables incluidas

# Define una lista de subredes
  subnets = [
    {
      subnet_name   = "${var.subnetwork}-${var.env_name}"  # Nombre de la subred con variables incluidas
      subnet_ip     = "10.10.0.0/16"  # Dirección IP de la subred
      subnet_region = var.region # Región definida como variable
    },
  ]

# Define los rangos utilizados en la red
  secondary_ranges = {
    "${var.subnetwork}-${var.env_name}" = [
      {
        range_name    = var.ip_range_pods_name # Primer elemento de la lista
        ip_cidr_range = "10.20.0.0/16" # Dirección IP del rango
      },
      {
        range_name    = var.ip_range_services_name # Segundo elemento de la lista
        ip_cidr_range = "10.30.0.0/16" # Dirección IP del rango
      },
    ]
  }
}

# Se declara un recurso de tipo "google_client_config" con el nombre "default" sin atributos.
data "google_client_config" "default" {}

# Se declara el proveedor "kubernetes".
provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"  # Se define la dirección host para el proveedor como el endpoint que se encuentra en la salida del módulo "gke".
  token                  = data.google_client_config.default.access_token # Se define el token de autenticación para el proveedor como el acces_token encontrado en la salida del recurso "google_client_config".
  cluster_ca_certificate = base64decode(module.gke.ca_certificate) # Se define el certificado CA del cluster para el proveedor como una decodificación base64 del certificado encontrado en la salida del módulo "gke".
}

# Se declara un módulo "gke".
module "gke" {
  source                 = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"   # Se define la fuente del módulo como un módulo en un repositorio en GitHub.
  version                = "24.1.0"  # Se define la versión del módulo.
  project_id             = var.project_id  # Se define el ID del proyecto para el módulo como una variable "project_id".
  name                   = "${var.cluster_name}-${var.env_name}"  # Se define el nombre del cluster para el módulo como una combinación de la variable "cluster_name" y la variable "env_name".
  regional               = true   # Se especifica que el módulo es un cluster regional.
  region                 = var.region  # Se define la región para el módulo como una variable "region".
  network                = module.gcp-network.network_name   # Se define la red para el módulo como el nombre de la red en la salida del módulo "gcp-network".
  subnetwork             = module.gcp-network.subnets_names[0]  # Se define la subred para el módulo como la primera entrada en la lista de nombres de subredes en la salida del módulo "gcp-network".
  ip_range_pods          = var.ip_range_pods_name  # Se define el rango de IP para pods para el módulo como una variable "ip_range_pods_name".
  ip_range_services      = var.ip_range_services_name  # Se define el rango de IP para servicios para el módulo como una variable "ip_range_services_name".
}